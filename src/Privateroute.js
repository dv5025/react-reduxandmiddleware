import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const Privateroute = ({ path, isLogin, exact, component: Component }) => (
    // * Use local storage
    // <Route path={path} exact={exact} render={(props) => {
    //     const isLogin = localStorage.getItem('isLogin');
    //     if (isLogin === 'true')
    //         return <Component {...props} />
    //     else
    //         return <Redirect to="/login" />
    // }} />

    // * Use Redux
    <Route path={path} exact={exact} render={(props) => {
        if (isLogin == true)
            return <Component {...props} />
        else
            return <Redirect to="/login" />
    }} />
);

const mapStateToProps = (state) => {
    return {
        isLogin: state.loginPage.isLogin
    }
}

export default connect(mapStateToProps)(Privateroute);
